<!DOCTYPE html>
<html class="no-js">
    
    <head>
    <link rel="icon" type="image/png" href="../image/skanic.jpg">
        <title>Website Inventory Sekolah</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Inventory Sekolah</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                                   </ul>
                        <ul class="nav">
                            <li class="active">
                                <a href="#">Dashboard</a>
                            </li>
                            <li>
                                <a href="login.php">Login</a>
                            </li>
                           
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="dashboard.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>                        
                    </ul>
                </div>
                
                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Selamat Datang</h4>
                        	Di Admin Inventory Sekolah</div>
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="dashboard.php">Kembali</a> <span class="divider">/</span>	
	                                    </li>
	                                    
	                                    <li class="active">Dashboard</li>
	                                </ul>
                            	</div>
                        	</div>
                            Latar Belakang Sistem informasi telah berkembang dalam bidang teknologi yang menyediakan kesempatan besar untuk berkembangnya teknologi Sistem Informasi. Berkembangnya teknologi informasi membuat pengaksesan terhadap informasi dituntut untuk cepat dan akurat agar terhindar dari resiko manipulasi data. Website simpan, pinjam, pengembalian inventaris saat ini mendapatkan perhatian dari sekolah. Website simpan, pinjam, pengembalian inventaris merupakan salah satu sektor pembangunan yang sangat potensial untuk dapat dikembangkan dengan teknologi informasi. Sistem informasi terkomputerisasi saat ini banyak diterapkan dan dituntut untuk mengatasi kecurangan dalam suatu sistem informasi. Berdasarkan hasil pengamatan dilapangan dan wawancara dengan salah satu pengurus, salah satu faktor yang menyebabkan masalah tersebut adalah sistem pencatatan dan penyimpanan data sistem belum maksimal, yaitu dengan penggunaan program aplikasi excel. Dengan menggunakan program aplikasi excel orang dapat mudah memanipulasi data dengan mudah karena orang sudah hampir banyak yang mengetahuinya. Pencatatan dilakukan dibeberapa bagian atau unit, data tersimpan dibeberapa tempat, sehubungan hal tersebut dapat memperlambat saat akan berkoordinasi untuk proses peminjaman, proses persetujuan sampai dengan proses pengembalian, begitu juga dalam hal perekapan data maupun laporan.
                            <br><br>
                            Untuk mengatasi permasalahan diatas, maka diusulkan sebuah sistem. Sistem yang akan dibuat adalah Otomatisasi untuk mengetahui kelayakan simpan, pinjam, pengembalian inventaris. Sistem tersebut diharapkan dapat mendukung kelancaran proses transaksi simpan, pinjam dan pengembalian inventaris, kemudian mempermudah membuat laporan-laporan yang ada pada sistem website tersebut. Sehingga diharapkan pada saat pergantian kepengurusan tidak ada lagi anggota yang enggan atau menolak terutama menjadi ketua, bendahara dan unit simpan, pinjam dan pengembalian website inventaris, karena anggota jenuh dengan membuat laporan secara manual dan menyita banyak waktu buat mereka. Dengan sistem yang baru juga diharapkan agar mekanisme dan proses perekapan simpanan lebih cepat, transaksi peminjaman lebih cepat, mulai dari pengisian formulir peminjaman, mengetahui persetujuan peminjaman sampai pada pencairan peminjam. Kemudian dapat mengetahui cara perhitungan jumlah peminjaman yang boleh diajukan anggota dapat dilihat setiap saat, jumlah stok barang sampai saat ini, begitu juga dengan pengajuan persetujuan peminjaman barang serta bukti data, simpanan dapat dilihat dan dilakukan prosesnya secara online.
                            <br><br>
                            Alamat	   :	   Jl. Desa Laladon No.21 Kecamatan Ciomas Kabupaten Bogor
                            <br>
                            Telepon	   :	   (0251) 7520933
                            <br>
                            Web	       :	   smkn1ciomas.com
                            <br>
                            Email	   :	   smkn1_ciomas@yahoo.co.id
                            <br>
                            Kode Pos   :	   16610
                            </div>
                   
                </div>
            </div>
            <hr>
            <center>
            <footer>
                <p>&copy; Inventory Sekolah@ 2019</p>
            </footer>
            </center>
        </div>
        <!--/.fluid-container-->
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            // Easy pie charts
            $('.chart').easyPieChart({animate: 1000});
        });
        </script>
    </body>

</html>