<?php
include ('cek.php');
?>
<?php
// include ('cek_level.php');
if($_SESSION['id_level'] !== "1" ){
    die("<script>alert('Mungkin Anda Tersesat!');document.location.href='index.php'</script>");
    
}
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
    <link rel="icon" type="image/png" href="../image/skanic.jpg">
        <title>Admin Inventory Sekolah</title>
        <!-- Bootstrap -->
         <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Inventory Sekolah</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i><?php echo ucwords($_SESSION['petugas']);?><i class="caret"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="profile.php">Profile</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="active">
                                <a href="index.php">Dashboard</a>
                            </li>
                           
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
            <?php
            
			if ($_SESSION['id_level']==1){
				
                echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-home"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="inventory.php"><i class="icon-briefcase"></i> Inventory</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-shopping-cart"></i> Peminjaman </a>
                        </li>
                        <li>
                            <a href="pengembalian.php"><i class="icon-retweet"></i> Pengembalian </a>
                        </li>
                        <li>
                            <a href="laporan.php"><i class="icon-book"></i> Laporan</a>
                        </li>
						<li class="active">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-list"></i> Master Data </a>
                        </a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="data_pegawai.php">Data Pegawai</a></li>
                        <li><a tabindex="-1" href="data_petugas.php">Data Petugas</a></li>
                        <li><a tabindex="-1" href="data_ruang.php">Data Ruang</a></li>
                        <li><a tabindex="-1" href="data_jenis.php">Data Jenis</a></li>
                    </li>
                    </ul>
                    </ul>
                </div>';
			}
			elseif ($_SESSION['id_level']==2){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-home"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-shopping-cart"></i> Peminjaman</a>
                        </li>
                      
                        <li>
                            <a href="pengembalian.php"><i class="icon-retweet"></i> Pengembalian</a>
                        </li>
						
                        
                    </ul>
                </div>';
			}
			elseif ($_SESSION['id_level']==3){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-home"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-shopping-cart"></i> Peminjaman</a>
                        </li>
                      
                      
                        
                    </ul>
                </div>';
			}
            ?>
                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Selamat Datang</h4>
                            Admin Inventory Sekolah</div>
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="index.php">Dashboard</a> <span class="divider">/</span>	
	                                    </li>
	                                    <li class="active">Home</li>
	                                </ul>
                            	</div>
                        	</div>
                    	</div>


                     <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Tabel Data Pegawai</div>
                            </div>

                            <div class="block-content collapse in">
                                <div class="span12">
                                    
  									<a href="tambah_pegawai.php" class="btn btn-primary fa fa-plus" style="margin-left: 15px;">+Tambah Pegawai</a>
									<br>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    
  									<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
									
										<thead>
											<tr>
												<th>Id Pegawai</th>
												<th>Nama Pegawai</th>
												<th>Nip</th>
												<th>Alamat</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
								
									<?php
                                    include "koneksi.php";
                                    $no=1;
                                    $select=mysql_query("select * from pegawai");
                                    while($data=mysql_fetch_array($select))
                                    {
                                    ?>
                                      <tr>
										<td><?php echo $data['id_pegawai']; ?></td>
										<td><?php echo $data['nama_pegawai']; ?></td>
										<td><?php echo $data['nip']; ?></td>
                                        <td><?php echo $data['alamat']; ?></td>								
										
										
											<td><a class="btn btn outline btn-primary fa fa-edit" href="edit_pegawai.php?id_pegawai=<?php echo $data['id_pegawai']; ?>">Edit</a>
											<a class="btn btn outline btn-danger fa fa-trash-o" href="hapus_pegawai.php?id_pegawai=<?php echo $data['id_pegawai']; ?>">Hapus</a></td>
	                                    
										 
											

                                        </tr>
										<?php
									}
									?>
											
										</tbody>
									</table>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

                     <div class="row-fluid">
                        <!-- block -->
                        
                        <!-- /block -->
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <center>
                    <p>&copy; Inventory Sekolah@ 2019</p>
                </center>
            </footer>
        </div>
        <!--/.fluid-container-->

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
    </body>

</html>