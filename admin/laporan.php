<?php  
include 'koneksi.php';
?>
<?php
include ('cek.php');
?>
<?php
// include ('cek_level.php');
if($_SESSION['id_level'] !== "1" ){
    die("<script>alert('Mungkin Anda Tersesat!');document.location.href='index.php'</script>");
    
}
?>
<?php
// if(isset($_POST['tanggal1'],$_POST['tanggal2'])){
    // session_start();
//     $tanggal1 = $_POST['tanggal1'];
//     $tanggal2 = $_POST['tanggal2'];
// $query=mysql_query("SELECT * from inventaris");
//     $cek=mysql_num_rows($query);
//     if ($cek > 0) {
//     $_SESSION['tanggal1'] = $_POST['tanggal1'];
//     $_SESSION['tanggal2'] = $_POST['tanggal2'];
// header("Location:laporan.php");
//     }
//     else
//     {
//     header("Location:laporan.php");
//     }
// }
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
    <link rel="icon" type="image/png" href="../image/skanic.jpg">
        <title>Admin Inventory Sekolah</title>
        <!-- Bootstrap -->
         <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Inventory Sekolah</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i>
                                <?php
                                echo ucwords($_SESSION['petugas']);
                                ?>	
                              <i class="caret"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="profile.php">Profile</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="active">
                                <a href="index.php">Dashboard</a>
                            </li>
                           
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
            <?php
            
			if ($_SESSION['id_level']==1){
				
                echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-home"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="inventory.php"><i class="icon-briefcase"></i> Inventory</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-shopping-cart"></i> Peminjaman </a>
                        </li>
                        <li>
                            <a href="pengembalian.php"><i class="icon-retweet"></i> Pengembalian </a>
                        </li>
                        <li class="active">
                            <a href="laporan.php"><i class="icon-book"></i> Laporan</a>
                        </li>
						<li  class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-list"></i> Master Data </a>
                        </a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="data_pegawai.php">Data Pegawai</a></li>
                        <li><a tabindex="-1" href="data_petugas.php">Data Petugas</a></li>
                        <li><a tabindex="-1" href="data_ruang.php">Data Ruang</a></li>
                        <li><a tabindex="-1" href="data_jenis.php">Data Jenis</a></li>
                    </li>
                    </ul>
                    </ul>
                </div>';
			}
			elseif ($_SESSION['id_level']==2){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                      
                        <li>
                            <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian</a>
                        </li>
						
                        
                    </ul>
                </div>';
			}
			elseif ($_SESSION['id_level']==3){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                      
                      
                        
                    </ul>
                </div>';
			}
            ?>
               <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Selamat Datang</h4>
                        	Di Admin Inventory Sekolah</div>
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="index.php">Dashboard</a> <span class="divider">/</span>	
	                                    </li>
	                                    <li class="active">Laporan</li>
	                                </ul>
                            	</div>
                        	</div>
                    	</div>
                        <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Report Data Inventory</div>
                            </div>
                            <form action="" method="get" role="form">
                                    <h3><center>Report Data Barang</center></h3><br>
                                        <div style="margin-left : 300px">
                                     <div class="form-group col-md-4">                              
                                        <label>Dari Tanggal</label>
                                <input type="date" autocomplete="off" class="form-control input-tanggal" name="tanggal1" placeholder="Pilih Tanggal">
                                        </div>
                                        </div>
                                            <div style="margin-left : 300px">
                                     <div class="form-group col-md-4">
                                        <label>Sampai Tanggal</label>
                                <input type="date" autocomplete="off" class="form-control input-tanggal" name="tanggal2" placeholder="Pilih Tanggal">
                                        </div>
                                        </div>
                                        <br>
                                        <div style="margin-left : 500px">
                                         <input type="submit" class="btn btn-primary" name="pilih" value="Pilih">
                                     </div>
                                     </form>
                                     
    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.input-tanggal').datepicker({dateFormat: 'yyy-dd-mm'});       
    });
</script>
            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                            <th>Tanggal Register</th>
                            <th>Kode Barang</th>
                        </tr>
                    </thead>
                    <tbody>
            <?php 
        include 'koneksi.php';
          
        $no = 1;
        $select = mysql_query("SELECT * from inventaris where tanggal_register ");
        if(isset($_GET['pilih'])){
            // echo "<script>alert('a')</script>";
            // var_dump($_POST);die();
            $tanggal1 = $_GET['tanggal1'];
            $tanggal2 = $_GET['tanggal2'];
            $select = mysql_query("SELECT * from inventaris where tanggal_register BETWEEN '$tanggal1' AND '$tanggal2' ");
            
        }
        while($data = mysql_fetch_array($select)){
            ?>
                    
                        <tr class="success">
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $data['nama'] ?></td>
                            <td><?php echo $data['jumlah'] ?></td>
                            <td><?php echo $data['tanggal_register']?></td>
                            <td><?php echo $data['kode_inventaris'] ?></td>
                        </tr>
                        <?php 
        }
        ?>
                </table>
        <?php if(isset($_GET['pilih'])){ ?>

                <a href="export_pdf_inventaris.php?tanggal1=<?php echo $_GET['tanggal1'].'&tanggal2='.$_GET['tanggal2'] ?>" class="btn btn-danger">Print To PDF</a>
                <a href="export_inventaris.php?tanggal1=<?php echo $_GET['tanggal1'].'&tanggal2='.$_GET['tanggal2'] ?>" class="btn btn-success">Export To EXCEL</a>
        <?php }else{ ?> 
                <a href="export_pdf_inventaris.php" class="btn btn-danger">Print To PDF</a>
                <a href="export_inventaris.php" class="btn btn-success">Export To EXCEL</a>
        <?php } ?>

    <script type ="text/javascript" src="assets/js/jquery.min.js"></script>
                                        <script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
                                        <script>$(document).ready(function(){
                                            $('#example').DataTable();
                                        });
                                        </script>
            </div>
            </div>
            </div>
            </div>
            </div>
                        <!-- block -->
                        
                        <!-- /block -->
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <center>
                    <p>&copy; Inventory Sekolah@ 2019</p>
                </center>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script>
        $(document).ready(function () {
            // [17, 74, 6, 39, 20, 85, 7]
            //[82, 23, 66, 9, 99, 6, 2]
            var data1 = [[gd(2012, 1, 1), 17], [gd(2012, 1, 2), 74], [gd(2012, 1, 3), 6], [gd(2012, 1, 4), 39], [gd(2012, 1, 5), 20], [gd(2012, 1, 6), 85], [gd(2012, 1, 7), 7]];

            var data2 = [[gd(2012, 1, 1), 82], [gd(2012, 1, 2), 23], [gd(2012, 1, 3), 66], [gd(2012, 1, 4), 9], [gd(2012, 1, 5), 119], [gd(2012, 1, 6), 6], [gd(2012, 1, 7), 9]];
            $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
                data1, data2
            ], {
                series: {
                    lines: {
                        show: false,
                        fill: true
                    },
                    splines: {
                        show: true,
                        tension: 0.4,
                        lineWidth: 1,
                        fill: 0.4
                    },
                    points: {
                        radius: 0,
                        show: true
                    },
                    shadowSize: 2
                },
                grid: {
                    verticalLines: true,
                    hoverable: true,
                    clickable: true,
                    tickColor: "#d5d5d5",
                    borderWidth: 1,
                    color: '#fff'
                },
                colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
                xaxis: {
                    tickColor: "rgba(51, 51, 51, 0.06)",
                    mode: "time",
                    tickSize: [1, "day"],
                    //tickLength: 10,
                    axisLabel: "Date",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 10
                        //mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]
                },
                yaxis: {
                    ticks: 8,
                    tickColor: "rgba(51, 51, 51, 0.06)",
                },
                tooltip: false
            });

            function gd(year, month, day) {
                return new Date(year, month - 1, day).getTime();
            }
        });
    </script>

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
    </body>

</html>