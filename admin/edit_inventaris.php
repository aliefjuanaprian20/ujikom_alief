<?php
include ('cek.php');
?>
<?php
// include ('cek_level.php');
if($_SESSION['id_level'] !== "1" ){
    die("<script>alert('Mungkin Anda Tersesat!');document.location.href='index.php'</script>");
    
}
?>

<?php
include "koneksi.php";
$id_inventaris=$_GET['id_inventaris'];

$select=mysql_query("select * from inventaris where id_inventaris='$id_inventaris'");
$data=mysql_fetch_array($select);
?>

<!DOCTYPE html>
<html class="no-js">
    
    <head>
    <link rel="icon" type="image/png" href="../image/skanic.jpg">
        <title>Admin Inventory Sekolah</title>
        <!-- Bootstrap -->
         <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Inventory Sekolah</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i><?php echo ucwords($_SESSION['petugas']);?><i class="caret"></i>
                               
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="profile.php">Profile</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                            <li>
                                <a href="index.php">Dashboard</a>
                            </li>
                           
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">       
            <?php
            
			if ($_SESSION['id_level']==1){
				
                echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-home"></i> Dashboard</a>
                        </li>
                        <li class="active">
                            <a href="inventory.php"><i class="icon-briefcase"></i> Inventory</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-shopping-cart"></i> Peminjaman </a>
                        </li>
                        <li>
                            <a href="pengembalian.php"><i class="icon-retweet"></i> Pengembalian </a>
                        </li>
                        <li>
                            <a href="laporan.php"><i class="icon-book"></i> Laporan</a>
                        </li>
                        <li  class="dropdown">
                            <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-list"></i> Master Data </a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="data_pegawai.php">Data Pegawai</a></li>
                        <li><a tabindex="-1" href="data_petugas.php">Data Petugas</a></li>
                        <li><a tabindex="-1" href="data_ruang.php">Data Ruang</a></li>
                        <li><a tabindex="-1" href="data_jenis.php">Data Jenis</a></li>
                    </li>
                    </ul>                       
                    </ul>
                </div>';
			}
			elseif ($_SESSION['id_level']==2){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-home"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-shopping-cart"></i> Peminjaman</a>
                        </li>
                      
                        <li>
                            <a href="pengembalian.php"><i class="icon-retweet"></i> Pengembalian</a>
                        </li>
						
                        
                    </ul>
                </div>';
			}
			elseif ($_SESSION['id_level']==3){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-home"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-shopping-cart"></i> Peminjaman</a>
                        </li>
                      
                      
                        
                    </ul>
                </div>';
			}
			?>
                
                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Selamat Datang</h4>
                        	Admin Inventory Sekolah</div>
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="inventory.php">Kembali</a>	
                                        </li>
                                        <li class="active"> / Edit Inventory</li>
	                                </ul>
                            	</div>
                        	</div>
                    	</div>

                    
					 <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Form Edit Inventaris</div>
								
                            </div>
							<br>
							
							
                            <div class="block-content collapse in">
                                <div class="span12">
                                  <form action="update_inventaris.php" method="post" class="form-horizontal">
                                      <fieldset>
                                       
                                      <div class="control-group">
                                          <label class="control-label" for="typeahead">Id Inventaris</label>
                                          <div class="controls">
                                            <input name="id_inventaris" type="text"  readonly class="span6" id="typeahead" 
											value="<?php echo $data['id_inventaris'];?>" 
											data-provide="typeahead" data-items="4">
                                            </div>
                                        </div>
                                       <div class="control-group">
                                          <label class="control-label" for="typeahead">Nama </label>
                                          <div class="controls">
                                            <input name="nama" type="text"  placeholder="Masukan Nama Inventaris" class="span6" id="typeahead" 
                                            value="<?php echo $data['nama'];?>"
                                            data-provide="typeahead" data-items="4" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Kondisi </label>
                                          <div class="controls">
                                            <input name="kondisi" type="text" placeholder="Masukan Tanggal Kembali" class="span6" id="typeahead"
                                            value="<?php echo $data['kondisi'];?>"
                                            data-provide="typeahead" data-items="4" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Keterangan </label>
                                          <div class="controls">
                                            <input name="keterangan" type="text"  placeholder="Masukan Status Peminjaman" class="span6" id="typeahead"  
                                            value="<?php echo $data['keterangan'];?>"
                                            data-provide="typeahead" data-items="4" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Jumlah </label>
                                          <div class="controls">
                                            <input name="jumlah" type="number"  id="typeahead"  
                                            value="<?php echo $data['jumlah'];?>"
                                            data-provide="typeahead" data-items="4" >
                                            </div>
                                        </div>
                                       <div class="control-group">
                                          <label class="control-label" for="typeahead">Id Jenis </label>
                                          <div class="controls">
                                          <select name="id_jenis" class="form-control m-boti5">
                                              <option><?php echo $data['id_jenis']?></option>
                                            <?php
                                            include "koneksi.php";
                                            $result = mysql_query("SELECT id_jenis,nama_jenis FROM jenis");
                                            while($row = mysql_fetch_assoc($result))
                                            {
                                                  echo "<option>$row[id_jenis].$row[nama_jenis]</option>";
                                             }
                                            ?>
                                        </select>
                                        </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Tanggal Register </label>
                                          <div class="controls">
                                            <input name="tanggal_register" type="text" id="typeahead" 
                                            data-provide="typeahead"
                                            value="<?php echo $data['tanggal_register'];?>"
											data-items="4" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Id Ruang </label>
                                          <div class="controls">
                                          <select name="id_ruang" class="form-control m-boti2">
                                              <option><?php echo $data['id_ruang']?></option>
                                             <?php
                                            include "koneksi.php";
                                            $result = mysql_query("SELECT id_ruang,nama_ruang FROM ruang");
                                            while($row = mysql_fetch_assoc($result))
                                            {
                                                  echo "<option>$row[id_ruang].$row[nama_ruang]</option>";
                                             }
                                            ?>
                                        </select>
                                        </div>   
                                        </div>                  
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Kode Inventaris</label>
                                          <div class="controls">
                                            <input name="kode_inventaris" type="text" placeholder="Masukan Kode Inventaris" class="span6" id="typeahead" 
                                            data-provide="typeahead"
                                            value="<?php echo $data['kode_inventaris'];?>"
                                            data-items="4" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Id Petugas </label>
                                          <div class="controls">
                                          <select name="id_petugas" class="form-control m-boti2">
                                              <option><?php echo $data['id_petugas']?></option>
                                             <?php
                                            include "koneksi.php";
                                            $result = mysql_query("SELECT id_petugas,nama_petugas FROM petugas");
                                            while($row = mysql_fetch_assoc($result))
                                            {
                                                  echo "<option>$row[id_petugas].$row[nama_petugas]</option>";
                                             }
                                            ?>
                                        </select>
                                        </div> 
										<button type="submit" class="btn btn-success">Simpan</button>
										<button type="reset" class="btn btn-danger">Reset</button>
                                       
                                      </fieldset>
                                    </form>
                                </div>
							</div>

						
						 <div class="row-fluid">
                        <!-- block -->
                        
                        <!-- /block -->
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <center>
                    <p>&copy; Inventory Sekolah@ 2019</p>
                </center>
            </footer>
        </div>
        <!--/.fluid-container-->

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
    </body>

</html>