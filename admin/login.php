<?php
include ('koneksi.php');

if(!isset($_SESSION)){
	session_start();
}

if(isset($_POST['username'])){
	$username = mysql_real_escape_string(addslashes(trim($_POST['username'])));
	$password = mysql_real_escape_string(trim($_POST['password']));

	$query = mysql_query("SELECT * FROM petugas WHERE username = '$username' AND password = '$password'");
	$data = mysql_fetch_array($query);
	$cek=mysql_num_rows($query);
	if ($cek>0)
	{
		$_SESSION['petugas'] = $data['nama_petugas'];
		$_SESSION['id_level'] =  $data['id_level'];
		header("location:successlogin.php");
	}
}
	
?>
<!DOCTYPE html>
<html lang="en">
  
<head>
<link rel="icon" type="image/png" href="../image/skanic.jpg">
    <meta charset="utf-8">
    <title>Login - Admin-Operator-Peminjam</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

<link href="../css/font-awesome.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link href="../css/pages/signin.css" rel="stylesheet" type="text/css">
<script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>


</head>

<body>
	
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="index.php">
				Login Website Inventaris Sekolah				
			</a>		
			
			
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->



<div class="account-container">
	
	<div class="content clearfix">
		
		<form action="" method="post">
		
			<h1>Login Website Inventaris</h1>		
			
			<div class="login-fields">
				
                <p>Tolong masukan detail akun anda</p>
				
				<div class="field">
					<label for="username">username</label>
					<input type="text" pattern="[A-Za-z]{8,0}" id="username" autocomplete="off" name="username" value="" placeholder="username" maxlength="20" class="login username-field" />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" id="password" name="password" value="" md5 placeholder="Password" maxlength="20" class="login password-field"/>
					
				</div> <!-- /password -->				
			</div> <!-- /login-fields -->
			<input type="checkbox" id="showPass"/> Show Password
			
                <button class="button btn btn-primary btn-large">Sign In</button>
				<?php
				if (isset($cek) && (!$cek))
				{
					echo 'Login gagal, username atau password salah<br><br>';
				}
				?>
				<div class="login-extra">
	<a href="#">Lupa Password</a>
</div> <!-- /login-extra -->
               				
			</div> <!-- .actions -->
			
			
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->





        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
        <script>
            $(function(){
                $("#showPass").click(function(){
                    if($("[name=password]").attr('type')=='password'){
                        $("[name=password]").attr('type','text');
                    }else{
                        $("[name=password]").attr('type','password');
                    }
                });
            });
        </script>
    </body>

</html>