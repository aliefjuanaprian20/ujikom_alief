<?php
include ('cek.php');
include ('koneksi.php');
?>

<html class="no-js">
    
    <head>
    <link rel="icon" type="image/png" href="../image/skanic.jpg">
        <title>Admin Inventory Sekolah</title>
        <!-- Bootstrap -->
         <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Inventory Sekolah</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i><?php echo ucwords($_SESSION['petugas']);?><i class="caret"></i>
                               </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="profile.php">Profile</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="active">
                                <a href="#">Dashboard</a>
                            </li>
                           
                        </ul>
                    </div>
                                        <!--/.nav-collapse -->
                </div>
            </div>
        </div>
<div class="container-fluid">
            <div class="row-fluid">
            <?php
            
			if ($_SESSION['id_level']==1){
				
                echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-home"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="inventory.php"><i class="icon-briefcase"></i> Inventory</a>
                        </li>
                        <li class="active">
                            <a href="peminjaman.php"><i class="icon-shopping-cart"></i> Peminjaman </a>
                        </li>
                        <li>
                            <a href="pengembalian.php"><i class="icon-retweet"></i> Pengembalian </a>
                        </li>
                        <li>
                            <a href="laporan.php"><i class="icon-book"></i> Laporan</a>
                        </li>
						<li  class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-list"></i> Master Data </a>
                        </a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="data_pegawai.php">Data Pegawai</a></li>
                        <li><a tabindex="-1" href="data_petugas.php">Data Petugas</a></li>
                        <li><a tabindex="-1" href="data_ruang.php">Data Ruang</a></li>
                        <li><a tabindex="-1" href="data_jenis.php">Data Jenis</a></li>
                    </li>
                    </ul>
                    </ul>
                </div>';
			}
			elseif ($_SESSION['id_level']==2){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-home"></i> Dashboard</a>
                        </li>
                        <li class="active">
                            <a href="peminjaman.php"><i class="icon-shopping-cart"></i> Peminjaman</a>
                        </li>
                      
                        <li>
                            <a href="pengembalian.php"><i class="icon-retweet"></i> Pengembalian</a>
                        </li>
						
                        
                    </ul>
                </div>';
			}
			elseif ($_SESSION['id_level']==3){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-home"></i> Dashboard</a>
                        </li>
                        <li class="active">
                            <a href="peminjaman.php"><i class="icon-shopping-cart"></i> Peminjaman</a>
                        </li>
                      
                      
                        
                    </ul>
                </div>';
			}
            ?>
        
               <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Selamat Datang</h4>
                        	Di Admin Inventory Sekolah</div>
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="index.php">Dashboard</a> <span class="divider">/</span>	
	                                    </li>
	                                    <li class="active">Peminjaman</li>
	                                </ul>
                            	</div>
                        	</div>
                    	</div>
                        
	
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                            Form Peminjaman                       
											</div>
                                            <div class="right_col" role="main">
                                            <div class="right_col" role="main">
        <div class="col-md-1"></div>
        <div class="col-md-10">
                <form class="form-inline" action="" method="GET" enctype="multipart/form-data">
                <div class="form-group">
                    <label>Nama Pegawai</label>
                    <select class="form-control" tabindex="-1" name="id_pegawai" >
                        <?php
                        $select = mysql_query("SELECT * FROM pegawai");
                        // var_dump($select2);die();
                        ?>
                        <?php while($a = mysql_fetch_array($select)){ ?>
                            <option value="<?php echo $a['id_pegawai'] ?>" <?=isset($_GET['id_pegawai']) ? ($a['id_pegawai'] == $_GET['id_pegawai'] ? 'selected' :'') : '' ?>><?php echo $a['nama_pegawai']?> </option>
                        <?php } ?>
                    </select>
                </div>
                <center><div class="form-group">
                    <label>Nama Barang</label>
                    <select class="form-control" tabindex="-1" name="id_inventaris" >
                        <?php
                        $select = mysql_query("SELECT * FROM inventaris");
                        ?>
                        <?php while($b = mysql_fetch_array($select)){ ?>
                            <option value="<?php echo $b['id_inventaris'] ?>"><?php echo $b['nama']?> </option>
                        <?php }?>
                    </select>
                </div></center><br>
                <center>
                    <button class="btn btn-primary" type="submit">Cek Barang</button>
                </center>
            </form>
            <?php
            if(isset($_GET['id_pegawai']) && isset($_GET['id_inventaris'])){?>
            <form action="proses_peminjaman.php" method="post" enctype="multipart/form-data">
               <?php
               include "koneksi.php";
               $id_inventaris=$_GET['id_inventaris'];
               $select=mysql_query("select * from inventaris where id_inventaris='$id_inventaris'");
               $data=mysql_fetch_array($select);
                ?>
                <br>
                <br>
                <div class="row">

                  <input name="id_pegawai" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $_GET['id_pegawai'];?>" autocomplete="off" maxlength="11" required="">
                  <input name="id_inventaris" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>" autocomplete="off" maxlength="11" required="">
                  <div class="span3">Nama Barang<input name="nama" type="text" style="height:27px;" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" readonly></div>
                  <div class="span3">Kode Barang<input name="kode_inventaris" type="text" style="height:27px;" class="form-control" value="<?php echo $data['kode_inventaris'];?>" autocomplete="off" maxlength="11" readonly></div>
                  <div class="span3">Jumlah Tersedia<input name="jumlah" type="text" style="height:27px;" class="form-control" value="<?php echo $data['jumlah'];?>" autocomplete="off" maxlength="11" required="" readonly></div>
                  <div class="span2">Jumlah Pinjam<input name="jumlah_pinjam" type="number" style="height:27px;" class="form-control" required="" placeholder="Jumlah" autocomplete="off" min="1" max="<?php echo $data['jumlah'];?>"></div>
                  <br><br><br>
                  <br><button type="submit" class="btn">Pinjam</button>
                  <br>
                  <br>
                </div>
              </form>

            <?php 
            $status_peminjaman="Pinjam";
            ?>
            <br><br>
            <div class="row">
              <div class="panel-body">
                  <div class="table-responsive">

                    <div class="col-lg-12">
                      <?php 
                      $tanggal_pinjam=date('Y-m-d H:i:s'); 
                      ?>
                      <div class="row" style="margin-left: 550px">
                        <div class="col-md-7">Tanggal Pinjam :<input name="tanggal_pinjam" readonly type="text" style="height:27px;" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $tanggal_pinjam;?>" autocomplete="off" maxlength="11"></div>
                    </div><br>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Peminjam</th>
                                <th>Nama Barang</th>
                                <th>Kode Barang</th>
                                <th>Jumlah Pinjam</th>
                                <th>Nama Pegawai</th>
                                <th>Option</th>

                            </tr> 
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $select=mysql_query("select t.*,p.*,i.*,t.jumlah from temp_peminjaman t JOIN inventaris i ON t.id_inventaris=i.id_inventaris JOIN pegawai p ON t.id_pegawai=p.id_pegawai where t.id_pegawai='$_GET[id_pegawai]'");
                            while($data=mysql_fetch_array($select))
                            {
                                ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=$data['nama_peminjam']?></td>
                                    <td><?=$data['nama']?></td>
                                    <td><?=$data['kode_inventaris']?></td>
                                    <td><?=$data['jumlah']?></td>
                                    <td><?=$data['nama_pegawai']?></td>
                                    <td>
                                    <center><a class="btn btn outline btn-info  glyphicon glyphicon-trash" href="hapus_temp_peminjaman.php?id=<?php echo $data['id'] ?>&id_pegawai=<?php echo $_GET['id_pegawai'] ?>&id_inventaris=<?php echo $_GET['id_inventaris'] ?>"><b>X</b></a></td></center>

                               </tr>

                                       <?php
                                   }
                                   ?>

                           </tbody>
                       </table>
                       <br>
                       <a href="checkout.php?id_pegawai=<?=$_GET['id_pegawai']?>" class="btn btn-warning">&nbsp;Pinjam</a>
                       <hr><br>
                       <br>

                <br>

            <br/>

        </div>
    </div>
    </div>
    </div>
              <?php } ?>
											
										</tbody>
									</table>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
            <hr>
            <footer>
                <center>
                    <p>&copy; Inventory Sekolah@ 2019</p>
                </center>
            </footer>
        </div>
        <!--/.fluid-container-->

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>

    </body>
</html>