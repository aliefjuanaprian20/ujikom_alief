<?php
include ('cek.php');
?>
    <!DOCTYPE html>
    <html lang="en">
<?php include "header.php"; ?>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Pepustakaan</title>

        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="style.css" rel="stylesheet">
        <link href="assets/css/simple-sidebar.css" rel="stylesheet">
        <script src="assets/js/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="assets/js/highcharts.js" type="text/javascript"></script>
        <script src="assets/js/exporting.js" type="text/javascript"></script>
        <script type="text/javascript">
            var chart1; // globally available
            $(document).ready(function() {
                chart1 = new Highcharts.Chart({
                    chart: {
                        renderTo: 'container',
                        type: 'column'
                    },
                    title: {
                        text: 'barang dengan jml terbanyak'
                    },
                    xAxis: {
                        categories: ['merk']
                    },
                    yAxis: {
                        title: {
                            text: 'jumlah'
                        }
                    },
                    series: [
                        <?php
// file koneksi php
$server = "localhost";
$username = "root";
$password = "";
$database = "ujikom";
mysql_connect($server,$username,$password) or die("Koneksi gagal");
mysql_select_db($database) or die("Database tidak bisa dibuka");
$sql   = "SELECT * from inventaris ORDER BY jumlah DESC LIMIT 10"; // file untuk mengakses ke tabel database
$query = mysql_query( $sql ) or die(mysql_error());
while($ambil = mysql_fetch_array($query)){
	$jenis=$ambil['jenis'];
	$sql_jumlah   = "SELECT max(jumlah) as jumlah from inventaris where jenis='$jenis'";
	$query_jumlah = mysql_query( $sql_jumlah ) or die(mysql_error());
	while( $data = mysql_fetch_array( $query_jumlah ) ){
	   $jumlahx = $data['jumlah'];
	  }

	  ?> {
                            name: '<?php echo $jenis; ?>',
                            data: [<?php echo $jumlahx; ?>]
                        },
                        <?php } ?>
                    ]
                });
            });

        </script>
    </head>
    <style type="text/css">
        th {
            font-family: 'roboto-bold';
        }

        .btn-sm {
            font-family: 'roboto-bold';
        }

        .link:hover {
            text-decoration: none;
        }

    </style>

    <body>

        <div id="wrapper">
		<div id="page-content-wrapper">
                    <div class="container-fluid" style="margin-top: 10px;">


                        <!-- /.row -->
                        
                        <!-- /.row -->

                        <!-- row -->
                        <div class="row">

                            <!-- .col -->
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div id="container" style="width: 100%; height: 400px; margin: 0 auto"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->

                        </div>
                        <!-- /row -->

                    </div>

                </div>
                <!-- Page Content Wrapper -->

        </div>
        <!-- Wrapper -->

        <!--    <script src="assets/js/jquery-1.12.3.min.js" type="text/javascript"></script>-->
        <script src="assets/js/bootstrap.min.js"></script>
        <script>
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });

        </script>
    </body>
<?php include "footer.php"; ?>
    </html>
