<?php
include ('cek.php');
?>
<?php
// include ('cek_level.php');
if($_SESSION['id_level'] !== "1" ){
    die("<script>alert('Mungkin Anda Tersesat!');document.location.href='index.php'</script>");
    
}
?>
<?php
include "koneksi.php";
$id_pegawai=$_GET['id_pegawai'];

$select=mysql_query("select * from pegawai where id_pegawai='$id_pegawai'");
$data=mysql_fetch_array($select);
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
    <link rel="icon" type="image/png" href="../image/skanic.jpg">
        <title>Admin Inventory Sekolah</title>
        <!-- Bootstrap -->
         <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Inventory Sekolah</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                          <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i><?php echo ucwords($_SESSION['petugas']);?><i class="caret"></i>
                        </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="profile.php">Profile</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="active">
                                <a href="index.php">Dashboard</a>
                            </li>
                           
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
            <?php
            
			if ($_SESSION['id_level']==1){
				
                echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="index.php"><i class="icon-home"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="inventory.php"><i class="icon-briefcase"></i> Inventory</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-shopping-cart"></i> Peminjaman </a>
                        </li>
                        <li>
                            <a href="pengembalian.php"><i class="icon-retweet"></i> Pengembalian </a>
                        </li>
                        <li>
                            <a href="laporan.php"><i class="icon-book"></i> Laporan</a>
                        </li>
						<li class="active">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-list"></i> Master Data </a>
                        </a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="data_pegawai.php">Data Pegawai</a></li>
                        <li><a tabindex="-1" href="data_petugas.php">Data Petugas</a></li>
                        <li><a tabindex="-1" href="data_ruang.php">Data Ruang</a></li>
                        <li><a tabindex="-1" href="data_jenis.php">Data Jenis</a></li>
                    </li>
                    </ul>
                    </ul>
                </div>';
			}
			elseif ($_SESSION['id_level']==2){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-home"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-shopping-cart"></i> Peminjaman</a>
                        </li>
                      
                        <li>
                            <a href="pengembalian.php"><i class="icon-retweet"></i> Pengembalian</a>
                        </li>
						
                        
                    </ul>
                </div>';
			}
			elseif ($_SESSION['id_level']==3){
				 echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-home"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-shopping-cart"></i> Peminjaman</a>
                        </li>
                      
                      
                        
                    </ul>
                </div>';
			}
            ?>
                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Selamat Datang</h4>
                        	Admin Inventory Sekolah</div>
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="data_pegawai.php">Kembali</a> <span class="divider">/</span>	
                                        </li>
                                        <li class="active">Edit Pegawai</li>
	                                </ul>
                            	</div>
                        	</div>
                    	</div>

                    
                    <div class="row-fluid">
                        <!-- block -->
                        
                        <!-- /block -->
                    </div>

                    <div class="row-fluid">
                        <!-- block -->
                       
                        <!-- /block -->
                    </div>

                    <div class="row-fluid">
                        <!-- block -->
                      
                        <!-- /block -->
                    </div>

                    <div class="row-fluid">
                        <!-- block -->
                     
                        <!-- /block -->
                    </div>

                    <div class="row-fluid">
                        <!-- block -->
                       
                        <!-- /block -->
                    </div>

                    <div class="row-fluid">
                        <!-- block -->
                        
                        <!-- /block -->
                    </div>
					 <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Form Edit Pegawai</div>
								
                            </div>
							<br>
							
							
                            <div class="block-content collapse in">
                                <div class="span12">
                                  <form action="update_pegawai.php" method="post" class="form-horizontal">
                                      <fieldset>
                                         <div class="control-group">
                                          <label class="control-label" for="typeahead">Id Pegawai</label>
                                          <div class="controls">
                                            <input name="id_pegawai" type="text"  readonly class="span6" id="typeahead" 
											value="<?php echo $data['id_pegawai'];?>" 
											data-provide="typeahead" data-items="4">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Nama Pegawai </label>
                                          <div class="controls">
                                            <input name="nama_pegawai" type="text"  placeholder="Masukan Nama Pegawai" class="span6" id="typeahead" 
                                            value="<?php echo $data['nama_pegawai'];?>"
											data-provide="typeahead" data-items="4" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Nip</label>
                                          <div class="controls">
                                            <input name="nip" type="text" class="span6" id="typeahead" 
											value="<?php echo $data['nip'];?>" 
											data-provide="typeahead" data-items="4">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Alamat </label>
                                          <div class="controls">
                                            <input name="alamat" type="text" placeholder="Masukan Alamat Pegawai" class="span6" id="typeahead"
                                            value="<?php echo $data['alamat'];?>"
											data-provide="typeahead" data-items="4" >
                                            </div>
                                        </div>
										<button type="submit" class="btn btn-success">Simpan</button>
										<button type="reset" class="btn btn-danger">Reset</button>
                                       
                                      </fieldset>
                                    </form>
                                </div>
							</div>
						</div>
						
						 <div class="row-fluid">
                        <!-- block -->
                        
                        <!-- /block -->
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; Inventory Sekolah@ 2019</p>
            </footer>
        </div>
        <!--/.fluid-container-->

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
    </body>

</html>